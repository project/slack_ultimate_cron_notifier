<?php

namespace Drupal\slack_ultimate_cron_notifier;

use Drupal\Core\Logger\RfcLoggerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\ultimate_cron\Logger\LogEntry;
use Psr\Log\LoggerInterface;

class WatchdogLogger implements LoggerInterface {
  use RfcLoggerTrait;
  use StringTranslationTrait;

  /**
   * Constructs a ProxyClass Drupal proxy object.
   *
   * @param \Psr\Log\LoggerInterface $original
   * @param \Drupal\slack_ultimate_cron_notifier\PostNotification $postNotification
   */
  public function __construct(protected LoggerInterface $original, protected PostNotification $postNotification) {
  }

  /**
   * {@inheritdoc}
   */
  public function log($level, $message, array $context = []): void {
    $this->original->log($level, $message, $context);
  }

  /**
   * {@inheritdoc}
   */
  public function catchMessages(LogEntry $log_entry): void {
    $this->original->catchMessages($log_entry);
  }

  /**
   * {@inheritdoc}
   */
  public function unCatchMessages(LogEntry $log_entry): void {
    // $log_entry lives as a local variable in CronJob::run(), it is not
    // passed to hooks and it isn't saved until after hook_cron_post_run()
    // so there is no ordinary way to get to it. Because of this, it
    // gets waylaid here.
    $this->postNotification->postNotification($log_entry);
    $this->original->unCatchMessages($log_entry);
  }

  /**
   * {@inheritdoc}
   */
  public function catchMessagesShutdownWrapper(): void {
    $this->original->catchMessagesShutdownWrapper();
  }

  /**
   * {@inheritdoc}
   */
  public function catchMessagesShutdown(LogEntry $log_entry): void {
    $this->original->catchMessagesShutdown($log_entry);
  }

}
