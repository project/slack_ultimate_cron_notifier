<?php

namespace Drupal\slack_ultimate_cron_notifier\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure Slack ultimate_cron notifier settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  const WEBHOOK_URL = 'webhook_url';

  const CONFIG_NAME = 'slack_ultimate_cron_notifier.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'slack_ultimate_cron_notifier_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [self::CONFIG_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form[self::WEBHOOK_URL] = [
      '#type' => 'textfield',
      '#title' => $this->t('The Slack webhook URL'),
      '#description' => $this->t('https://hooks.slack.com/services/...'),
      '#default_value' => $this->config(self::CONFIG_NAME)->get(self::WEBHOOK_URL),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (!str_starts_with($form_state->getValue(self::WEBHOOK_URL), 'https://hooks.slack.com/services/')) {
      $form_state->setErrorByName('example', $this->t('This is not a Slack webhook URL, it should start with https://hooks.slack.com/services/'));
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config(self::CONFIG_NAME)
      ->set(self::WEBHOOK_URL, $form_state->getValue(self::WEBHOOK_URL))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
