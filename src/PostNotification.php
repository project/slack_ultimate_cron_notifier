<?php

namespace Drupal\slack_ultimate_cron_notifier;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\slack_ultimate_cron_notifier\Form\SettingsForm;
use Drupal\ultimate_cron\Logger\LogEntry;
use GuzzleHttp\Client;

class PostNotification {

  /**
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected KeyValueStoreInterface $kv;

  /**
   * @var string|null
   */
  protected ?string $url;

  /**
   * Constructs a ProxyClass Drupal proxy object.
   *
   * @param \GuzzleHttp\Client $httpClient
   * @param \Drupal\Core\KeyValueStore\KeyValueFactoryInterface $key_value_factory
   */
  public function __construct(protected Client $httpClient, KeyValueFactoryInterface $key_value_factory, ConfigFactoryInterface $config_factory) {
    $this->kv  = $key_value_factory->get('slack_ultimate_cron_notifier');
    $this->url = $config_factory->get(SettingsForm::CONFIG_NAME)->get(SettingsForm::WEBHOOK_URL);
  }

  /**
   * {@inheritdoc}
   */
  public function postNotification(LogEntry $log_entry): void {
    if (!$this->url) {
      return;
    }
    $severity = (int) $log_entry->severity;
    $name = $log_entry->name;
    $previous_severity = $this->kv->get($name);
    if (!in_array($severity, [RfcLogLevel::INFO, RfcLogLevel::DEBUG]) && $severity !== $previous_severity) {
      $levels = RfcLogLevel::getLevels();
      $status = $levels[$severity] ?? 'unknown';
      $previous_status = $levels[$previous_severity] ?? 'unknown';
      $this->httpClient->post(
        $this->url,
        [
          'body' => json_encode(['text' => sprintf('%s job status changed from %s to %s', $name, $previous_status, $status)]),
          'headers' => ['Content-type' => 'application/json'],
        ]
      );
      $this->kv->set($name, $severity);
    }
  }

}
