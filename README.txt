Set up a webhook as described at https://api.slack.com/messaging/webhooks
and copy it to the configuration at /admin/config/system/cron/slack .
