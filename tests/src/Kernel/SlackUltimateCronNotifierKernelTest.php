<?php

namespace Drupal\tests\slack_ultimate_cron_notifier\Kernel;

use Drupal\Core\Logger\RfcLogLevel;
use Drupal\KernelTests\KernelTestBase;
use Drupal\slack_ultimate_cron_notifier\Form\SettingsForm;
use Drupal\ultimate_cron\Entity\CronJob;

/**
 *
 */
class SlackUltimateCronNotifierKernelTest extends KernelTestBase {

  static int $severity;

  public static $modules = [
    'ultimate_cron',
    'slack_ultimate_cron_notifier',
  ];

  /**
   *
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installSchema('ultimate_cron', ['ultimate_cron_log', 'ultimate_cron_lock']);
    $this->installEntitySchema('ultimate_cron_job');
    $GLOBALS['config'][SettingsForm::CONFIG_NAME][SettingsForm::WEBHOOK_URL] = getenv('SLACK_ULTIMATE_CRON_NOTIFIER_WEBHOOK_URL');
  }

  /**
   *
   */
  public function testNotification() {
    $job = CronJob::create([
      'title' => 'test ' . date(DATE_RFC2822),
      'id' => 'test',
      'callback' => static::class . '::runJob',
    ]);
    // static::$severity = -1;
    //    $job->run();
    //    $this->assertSame(static::$severity, (int) $job->loadLatestLogEntry()->severity);
    //    static::$severity = RfcLogLevel::INFO;
    //    $job->run();
    //    $this->assertSame(static::$severity, (int) $job->loadLatestLogEntry()->severity);
    static::$severity = RfcLogLevel::ERROR;
    $job->run();
    $this->assertSame(static::$severity, (int) $job->loadLatestLogEntry()->severity);
  }

  /**
   *
   */
  public static function runJob() {
    \Drupal::logger('test')->log(static::$severity, 'test');
  }

}
